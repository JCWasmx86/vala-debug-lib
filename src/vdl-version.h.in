/* vdl-version.h.in
 *
 * Copyright 2022 jonathan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(VDL_INSIDE) && !defined(VDL_COMPILATION)
# error "Only <vdl.h> can be included directly."
#endif

/**
 * SECTION:vdlversion
 * @short_description: vdl version checking
 *
 * vdl provides macros to check the version of the library
 * at compile-time
 */

/**
 * VDL_MAJOR_VERSION:
 *
 * vdl major version component (e.g. 1 if %VDL_VERSION is 1.2.3)
 */
#define VDL_MAJOR_VERSION (@MAJOR_VERSION@)

/**
 * VDL_MINOR_VERSION:
 *
 * vdl minor version component (e.g. 2 if %VDL_VERSION is 1.2.3)
 */
#define VDL_MINOR_VERSION (@MINOR_VERSION@)

/**
 * VDL_MICRO_VERSION:
 *
 * vdl micro version component (e.g. 3 if %VDL_VERSION is 1.2.3)
 */
#define VDL_MICRO_VERSION (@MICRO_VERSION@)

/**
 * VDL_VERSION
 *
 * vdl version.
 */
#define VDL_VERSION (@VERSION@)

/**
 * VDL_VERSION_S:
 *
 * vdl version, encoded as a string, useful for printing and
 * concatenation.
 */
#define VDL_VERSION_S "@VERSION@"

#define VDL_ENCODE_VERSION(major,minor,micro) \
        ((major) << 24 | (minor) << 16 | (micro) << 8)

/**
 * VDL_VERSION_HEX:
 *
 * vdl version, encoded as an hexadecimal number, useful for
 * integer comparisons.
 */
#define VDL_VERSION_HEX \
        (VDL_ENCODE_VERSION (VDL_MAJOR_VERSION, VDL_MINOR_VERSION, VDL_MICRO_VERSION))

/**
 * VDL_CHECK_VERSION:
 * @major: required major version
 * @minor: required minor version
 * @micro: required micro version
 *
 * Compile-time version checking. Evaluates to %TRUE if the version
 * of vdl is greater than the required one.
 */
#define VDL_CHECK_VERSION(major,minor,micro)   \
        (VDL_MAJOR_VERSION > (major) || \
         (VDL_MAJOR_VERSION == (major) && VDL_MINOR_VERSION > (minor)) || \
         (VDL_MAJOR_VERSION == (major) && VDL_MINOR_VERSION == (minor) && \
          VDL_MICRO_VERSION >= (micro)))
